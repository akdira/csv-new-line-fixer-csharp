﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CsvFixer.WinForm
{
    public partial class Start : Form
    {

        public string Separator = "|";

        public Start()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            browseFile.ShowDialog();
        }

        private void browseFile_FileOk(object sender, CancelEventArgs e)
        {

            saveFile.ShowDialog();

        }


        private void saveFile_FileOk(object sender, CancelEventArgs e)
        {

            asyncCSV.RunWorkerAsync();
        }

        private void asyncCSV_DoWork(object sender, DoWorkEventArgs e)
        {
            Action action;
            var text = "";
            Stream myStream = null;

            var pathToFile = browseFile.FileName;
            var saveToFile = saveFile.FileName;


            // Begin spliting every row
            action = () => txtFilename.Text = "From: "+ browseFile.FileName + ". To: "+ saveFile.FileName;
            txtFilename.Invoke(action);


            // Clear log first
            action = () => txtLog.ResetText();
            txtLog.Invoke(action);


            // Add to log 
            action = () => txtLog.AppendText("Reading file \r\n");
            txtLog.Invoke(action);


            // Read file
            try
            {
                if ((myStream = browseFile.OpenFile()) != null)
                {
                    using (myStream)
                    {
                        using (StreamReader sr = new StreamReader(pathToFile))
                        {
                            text = sr.ReadToEnd();//all text wil be saved in text enters are also saved
                        }

                        // Insert code to read the stream here.
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // Trigger action
            var csvString = FixCsv(text);

            // Write file to text
            File.WriteAllText(saveToFile, csvString);



            // Begin spliting every row
            action = () => txtLog.AppendText("All done \r\n");
            txtLog.Invoke(action);



        }

        public List<string> _FixCsv(string text)
        {
            Action action;
            Stream myStream = null;


            // Begin spliting every row
            action = () => txtLog.AppendText("Spliting every row \r\n");
            txtLog.Invoke(action);

            string[] stringSeparators = new string[] { "\r\n", "\r", "\n" };
            var CsvLinesOnly = text.Split(stringSeparators, StringSplitOptions.None).ToList();
            var CsvLinesWithColumn = new List<List<string>>();




            // Begin spliting every row
            action = () => txtLog.AppendText("Spliting every column \r\n");
            txtLog.Invoke(action);

            for (var i = 0; i < CsvLinesOnly.Count; i++)
            {
                CsvLinesWithColumn.Add(CsvLinesOnly[i].Split(this.Separator.ToCharArray()).ToList());
            }

            if (CsvLinesOnly.Count == 0)
            {

                // There is no row, nothing to do
                action = () => txtLog.AppendText("There is no row, nothing to do \r\n");
                txtLog.Invoke(action);
            }

            // Checking header column
            action = () => txtLog.AppendText("Checking header column \r\n");
            txtLog.Invoke(action);


            // Declare var
            int headerCount = CsvLinesWithColumn[0].Count();
            var rowToRemove = new List<int>();
            var rowToMerge = new List<int>();

            // Total Header Column
            action = () => txtLog.AppendText("Total Header Column is " + headerCount.ToString() + " \r\n");
            txtLog.Invoke(action);

            // Begin checking new line every row
            action = () => txtLog.AppendText("Begin checking new line every row \r\n");
            txtLog.Invoke(action);



            for (var i = 0; i < CsvLinesWithColumn.Count; i++)
            {

                // Checking at line
                action = () => txtLog.AppendText("Checking at line #" + (i + 1) + " \r\n");
                txtLog.Invoke(action);

                var bottomToMerge = 0;

                while (CsvLinesWithColumn[i].Count < headerCount)
                {


                    bottomToMerge++;

                    // Break if overflow
                    if (i + bottomToMerge > CsvLinesWithColumn.Count - 1)
                        break;

                    // Mark row to merge
                    if (bottomToMerge == 1)
                        rowToMerge.Add(i + bottomToMerge);

                    // Check array index to prevent overflow column
                    var i_first_row_last_column = CsvLinesWithColumn[i].Count - 1;

                    // Merge bottom row to upper row
                    CsvLinesWithColumn[i] = CsvLinesWithColumn[i].Concat(CsvLinesWithColumn[i + bottomToMerge]).ToList();

                    // Mark bottom row to remove it later
                    rowToRemove.Add(i + bottomToMerge);

                    // Join duplicate column as one column
                    CsvLinesWithColumn[i][i_first_row_last_column] = CsvLinesWithColumn[i][i_first_row_last_column].Trim() + " " + CsvLinesWithColumn[i][i_first_row_last_column + 1].Trim();
                    //$csv_original_rows[$i][$i_first_row_last_column] = $csv_original_rows[$i][$i_first_row_last_column]. $csv_original_rows[$i][$i_first_row_last_column + 1];



                    // Delete after join
                    CsvLinesWithColumn[i].RemoveAt(i_first_row_last_column + 1);
                    //unset($csv_original_rows[$i][$i_first_row_last_column + 1]);
                }


                if (bottomToMerge != 0)
                {
                    i = i + bottomToMerge;

                    // Checking at line
                    action = () => txtLog.AppendText("Found, bottom to merge =  #" + (i + 1) + " \r\n");
                    txtLog.Invoke(action);
                }
            }


            // Show row to merge
            if (rowToMerge.Count > 0)
            {
                // Found, merged row
                action = () => txtLog.AppendText("Found, merged row \r\n");
                txtLog.Invoke(action);

                foreach (var oneRow in rowToMerge)
                {
                    // At line
                    action = () => txtLog.AppendText("At line #" + (oneRow + 1) + " \r\n");
                    txtLog.Invoke(action);

                }

            }


            // Sort array 9 to 1
            rowToRemove.Sort();
            rowToRemove.Reverse();

            foreach (var iOne in rowToRemove)
            {
                CsvLinesWithColumn.RemoveAt(iOne);
            }

            // Begin export to CSV
            var NewCsvString = "";
            var ReturnList = new List<string>();

            for (var i = 0; i < CsvLinesWithColumn.Count; i++)
            {
                var NewCsvLine = String.Join(this.Separator, CsvLinesWithColumn[i].Select(x => x.ToString()).ToArray());
                //NewCsvString += NewCsvLine + "\r\n";
                ReturnList.Add(NewCsvLine);

            }

            NewCsvString = NewCsvString.Trim();
            return ReturnList;

            //}
            //catch(Exception Error)
            //{
            //    MessageBox.Show("Error: " + Error.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}

        }

        public string FixCsv(string text)
        {
            var listLine = _FixCsv(text);
            var NewCsvString = "";

            foreach(var oneRow in listLine)
            {
                NewCsvString += oneRow + "\r\n";
            }

            return NewCsvString;

        }

        private void asyncCSV_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
        }

    }
}
